// framework express aprendiendo

//se incluye el modulo de express que esta en la carpeta node_modules del proyecto
//con la funcion require('express');
//se asigna a una variable llamada express para usarla en el proyecto
let express = require('express');

//se inicializa express para poder usarlo
let app = express();

//app.get -> el navegador hace una peticion al servidor lo hace atraves de http get
//cuando el servidor recibe la peticion get a la ruta inicial se trata como una funcion
//req -> request (peticion) lo que el navegador pide
//res -> respond (responder) lo que el servidor responde

//respond with "hello world" when a GET request is made to the homepage
app.get('/', function(req, res) {
  res.send('hello world!!');
});

//crea una escucha en el puerto 8080, sirve para ver en el navegador el servidor
//creado por medio de la direccion localhost:8080
app.listen(8080);